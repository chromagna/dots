let mapleader = ","

call plug#begin('~/.local/share/nvim/plugged')
Plug 'sainnhe/edge'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/vim-easy-align'
Plug 'antoinemadec/coc-fzf'
Plug 'tomtom/tcomment_vim'
Plug 'billyvg/tigris.nvim', {'do': './install.sh' }
Plug 'ekalinin/Dockerfile.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'Konfekt/FastFold'
Plug 'simeji/winresizer'
Plug 'jiangmiao/auto-pairs'
Plug 'cespare/vim-toml'
Plug 'pechorin/any-jump.vim'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'pangloss/vim-javascript'
Plug 'yuezk/vim-js'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'vifm/vifm.vim'
Plug 'itchyny/lightline.vim'
Plug 'niklaas/lightline-gitdiff'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'fatih/vim-go'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'shime/vim-livedown'
Plug 'kshenoy/vim-signature'
Plug 'projekt0n/github-nvim-theme'
Plug 'pappasam/coc-jedi', { 'do': 'npm install && npm run build', 'branch': 'main'  }
Plug 'ncm2/float-preview.nvim'
Plug 'stsewd/fzf-checkout.vim'
Plug 'tpope/vim-rhubarb'
" Plug 'unblevable/quick-scope'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'KabbAmine/vCoolor.vim'
Plug 'tyru/open-browser.vim'
Plug 'tyru/open-browser-github.vim'
Plug 'leafOfTree/vim-vue-plugin'
Plug 'bluz71/vim-moonfly-colors'
Plug 'marcopaganini/termschool-vim-theme'
" Plug 'kien/tabman.vim'
Plug 'gkjgh/cobalt'
Plug 'heavenshell/vim-jsdoc', {
  \ 'for': ['javascript', 'javascript.jsx','typescript'],
  \ 'do': 'make install'
\}
Plug 'github/copilot.vim'
" Plug 'wellle/context.vim'
" Plug 'karb94/neoscroll.nvim'
Plug 'rhysd/accelerated-jk'
Plug 'liuchengxu/vista.vim'
Plug 'dstein64/nvim-scrollview'
call plug#end()

if has("termguicolors")
    set t_8f=\[[38;2;%lu;%lu;%lum
    set t_8b=\[[48;2;%lu;%lu;%lum
    set termguicolors
endif

set t_Co=256
set background=light
colorscheme github_light
" colorscheme moonfly
" colorscheme termschool
highlight Normal ctermbg=NONE
highlight nonText ctermbg=NONE


" https://github.com/vim/vim/issues/981
hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE
hi! SignColumn ctermbg=NONE guibg=NONE
hi! LineNr ctermbg=NONE guibg=NONE

" https://github.com/weirongxu/coc-explorer/issues/89
augroup OpenAllFoldsOnFileOpen
    autocmd!
    autocmd BufRead * normal zR
augroup END

" float-preview.nvim
let g:float_preview#docked = 1

" fzf preview layout window
" this is the default, but leave it here for future configuration if desired
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" UltiSnips requires a python provider to be defined.
let g:python3_host_prog = "/usr/local/bin/python3"
let g:python_host_prog = "/Users/pyers/.pyenv/shims/python"

" QuickScope only highlight letters when f, F, t or T is pressed instead of
" all the time
" let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']


" lightline
let g:lightline = {
      \ 'colorscheme': '16color',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'gitbranch', 'filename', 'readonly', 'modified' ], [ 'gitdiff', 'cocstatus' ] ],
      \   'right': [ [ 'lineinfo' ], [ 'percent' ] ],
      \ },
      \ 'tabline': {
      \   'left': [ ['tabs'] ],
      \   'right': [ ['close'] ],
      \ },
      \ 'inactive': {
      \   'left': [ [ 'filename', 'gitversion' ] ],
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#Head',
      \   'cocstatus': 'coc#status',
      \ },
      \ 'component_expand': {
      \   'gitdiff': 'lightline#gitdiff#get',
      \   'buffers': 'lightline#bufferline#buffers',
      \ },
      \ 'component_type': {
      \   'gitdiff': 'middle',
      \   'buffers': 'tabsel',
      \ },
      \ }

let g:lightline#gitdiff#indicator_added = 'A: '
let g:lightline#gitdiff#indicator_deleted = 'D: '
let g:lightline#gitdiff#separator = ' '

let g:lightline.mode_map = {
            \ 'n': 'NRM',
            \ 'i': 'INS',
            \ 'R': 'RPL',
            \ 'v': 'VIS',
            \ 'V': 'VLN',
            \ "\<C-v>": 'VBK',
            \ 'c': 'CMD',
            \ 's': 'SEL',
            \ 'S': 'SLN',
            \ "\<C-s>": 'SBK',
            \ 't': 'TRM',
            \ }

" fzf
let g:fzf_files_options = "--preview 'echo {}' --bind '?:toggle-preview' --border"

" better git keymappings
" open git status buffer
nnoremap <leader>gg :Git<CR>

" search and manipulate commit history
nnoremap <leader>gc :Commits<CR>
" search and manipulate branches
nnoremap <leader>gb :GBranches<CR>
" open in browser
nnoremap <leader>gB :GBrowse<CR>
" open visual selection in browser
vnoremap <leader>gB :GBrowse<CR>
" open diff split
nnoremap <leader>gd :Gdiff<CR>
" stage and unstage hunks
nnoremap <leader>ghs <Plug>(GitGutterStageHunk)
nnoremap <leader>ghu <Plug>(GitGutterUndoHunk)
vnoremap <leader>ghs <Plug>(GitGutterStageHunk)
vnoremap <leader>ghu <Plug>(GitGutterUndoHunk)


filetype plugin on
syntax on

set hidden
set nowrap
set mouse=a
set number
set hlsearch
set nobackup
" set nonumber
set autowrite
set expandtab
set incsearch
set showmatch
set cursorline
set ignorecase
set noshowmode
" set smartcase
set cmdheight=1
set foldenable!
set linespace=0
set scrolloff=7
set cursorcolumn
set laststatus=2
set nocompatible
set shortmess+=c
set nowritebackup
set softtabstop=2
set nocursorcolumn
set signcolumn=no
set updatetime=100
set foldmethod=indent
set tabstop=2 " show existing tab with 2 spaces width
set shiftwidth=2 " when indenting with >, use 2 spaces width

" Natural split openings
set splitbelow
set splitright

" More intuitive split navigation
noremap <C-h> <C-w><C-h>
noremap <C-j> <C-w><C-j>
noremap <C-k> <C-w><C-k>
noremap <C-l> <C-w><C-l>

" More intuitive tab navigation
nnoremap <S-J> gT
nnoremap <S-K> gt

" Open explorer
nnoremap \ :CocCommand explorer<CR>

" If wrapping is enabled.. then moving up and down should be wrap-aware.
nnoremap j gj
nnoremap k gk

" Misc leader bindings that are probably unnecessary but I keep them for
" convenience sake.
nnoremap <leader>n :noh<CR>
nnoremap Y y$

" make Ag search only file contents, not filenames
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, {'options': '--delimiter : --nth 4..'}, <bang>0)

nmap <leader>c :CocFzfList<CR>
" nnoremap <c-p> :Files<CR>
" nnoremap <leader>, :Files<CR>
nnoremap <c-p> :GFiles<CR>
nnoremap <leader>, :Files<CR>
nnoremap <silent> <leader>AG :Ag <C-R><C-A><CR>
" nnoremap <silent> <leader>ag y:Ag <C-R>"<CR>
nnoremap <silent> <leader>ag :Ag<CR>

nmap <leader>r <Plug>(coc-rename)

" Use a specific version of node?
" https://neovim.io/doc/user/provider.html
let g:node_host_prog='/usr/local/n/versions/node/17.1.0/bin/node'
let g:coc_node_path='/usr/local/n/versions/node/17.1.0/bin/node'


" Easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap <leader>a <Plug>(EasyAlign)
" Ignoring delimiters in comments or strings
let g:easy_align_ignore_groups = ['Comment', 'String']

" Sort lines by length
command SortSelectedLinesAscending  :'<,'>! awk '{ print length(), $0 | "sort -n | cut -d\\  -f2-" }'
command SortSelectedLinesDescending :'<,'>! awk '{ print length(), $0 | "sort -nr | cut -d\\  -f2-" }'
vnoremap <silent> <leader>sa       :<C-U>SortSelectedLinesAscending<CR>
vnoremap <silent> <leader>sd       :<C-U>SortSelectedLinesDescending<CR>

" NameAssign
vmap <leader>b <Plug>NameAssign

" Automatically reload file when it changes on disk
set autoread
au CursorHold * checktime

" Terminal mode
" This is commented out because it kinda ruins the fzf preview window
" experience.
" :tnoremap <Esc> <C-\><C-n>
" Instead, let's map ALT to close terminal focus
:tnoremap <M-n> <C-\><C-n>
:tnoremap <C-x> <C-\><C-n>

if has('nvim')
    highlight! link TermCursor Cursor
    highlight! TermCursorNC guibg=red guifg=white ctermbg=1 ctermfg=15
endif

" Autopairs - turn off 'fly mode'
let g:AutoPairsFlymode=0
let g:AutoPairsCenterLine=0

" Moving lines around.
" https://vi.stackexchange.com/a/2682
nnoremap <S-Up> :m .-2<CR>==
nnoremap <S-Down> :m .+1<CR>==
inoremap <S-Up> <Esc>:m .-2<CR>==gi
inoremap <S-Down> <Esc>:m .+1<CR>==gi
vnoremap <S-Up> :m '<-2<CR>gv=gv
vnoremap <S-Down> :m '>+1<CR>gv=gv

" Persistent undo
if has('persistent_undo')
    set undofile
    set undodir=~/.config/vim/tmp/undo//
endif

" swap file location - messing with file reloads using nodejs's fs.watch:
set dir=$HOME/.vim/tmp/swap
if !isdirectory(&dir) | call mkdir(&dir, 'p', 0700) | endif

" Livedown
let g:livedown_autoren = 0
let g:livedown_open = 1
let g:livedown_port = 1337

" ----------------------------------------------------------------------------------
" ----------------------------------------------------------------------------------
" ----------------------------------------------------------------------------------
"  coc.nvim

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gf <Plug>(coc-fix-current)

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
" nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup UpdateSignatureHelpOnJump
  autocmd!
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" ----------------------------------------------------------------------------------
" ----------------------------------------------------------------------------------
" ----------------------------------------------------------------------------------
"  Go
let g:go_def_mapping_enabled = 0 " let coc.nvim handle definition mapping
let g:go_fmt_autosave = 1 " Enabled by default
let g:go_fmt_command = "gopls"
let g:go_list_type = "quickfix"
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1
let g:go_auto_type_info = 1
let g:go_decls_includes = "func,type"
let g:go_doc_keywordprg_enabled = 0 " Disable in favor of coc.nvim
" let g:go_doc_max_height = 20 " Not used since we're showing inside a balloon
" let g:go_doc_balloon = 1 " Show GoDoc inside of a balloon
" let g:go_doc_popup_window = 1
let g:go_gopls_use_placeholders = 1
let g:go_snippet_engine = "ultisnips"
let g:go_updatetime = 200
let g:go_term_mode = "vsplit"

autocmd BufWritePost *.go :GoBuild

" The snippets bundled with vim-go have proven to be extremely handy.
let g:UltiSnipsExpandTrigger="<s-tab>"
let g:UltiSnipsJumpForwardTrigger="<s-tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-tab>"

autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
" autocmd FileType go nmap <leader>c <Plug>(go-coverage-toggle)
autocmd FileType go nmap <leader>t <Plug>(go-test)
" autocmd FileType go nmap <leader>b <Plug>(go-build)
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>s :GoFillStruct<CR>
" :GoDoc is useful if you need to leave a doc preview window open at the
" bottom of the terminal.
autocmd FileType go nmap <leader>d :GoDoc<CR>
"
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Show documentation for word under cursor on 'H'
nnoremap H :call <SID>show_documentation()<CR>

" Add missing imports on save
" Alternatively, run quickfix on the line with 'gf'
autocmd BufWritePre *.go :silent call CocAction('runCommand', 'editor.action.organizeImport')

" default wellle/context.vim mappings, but no 'H'
" let g:context_add_autocmds = 0
" if !exists('##WinScrolled')
"   nnoremap <silent> <expr> <C-Y> context#util#map('<C-Y>')
"   nnoremap <silent> <expr> <C-E> context#util#map('<C-E>')
"   nnoremap <silent> <expr> zz    context#util#map('zz')
"   nnoremap <silent> <expr> zb    context#util#map('zb')
" endif
" nnoremap <silent> <expr> zt    context#util#map_zt()
" nnoremap <silent> <expr> H     context#util#map_H()
" lua require('neoscroll').setup()

nmap j <Plug>(accelerated_jk_gj)
nmap k <Plug>(accelerated_jk_gk)

let g:copilot_no_tab_map = 1
inoremap <silent> <expr> <C-l> copilot#Accept()

" Highlighting for coc.nvim diagnostic messages.
hi! CocErrorSign guibg=#fee4e4 guifg=#ff0000
hi! CocInfoSign guibg=#e2e2e2 guifg=#444444
hi! CocWarningSign guibg=#ffefd7 guifg=#d59f4d

" Use tab key to go to next tab.
nnoremap <silent> <Tab> :tabnext<CR>
" Use shift tab key to go to previous tab.
nnoremap <silent> <S-Tab> :tabprevious<CR>
