# dots

## Quickstart

- My preferred font which is referenced by kitty.conf: https://www.jetbrains.com/lp/mono/

### For neovim

To install the plugins defined in `init.vim`:

```bash
$ nvim +PlugInstall +qall
```

To install the coc extensions I'm using at the moment:

```bash
$ mkdir -p ~/.config/coc/extensions
$ cd ~/.config/coc/extensions
$ npm install coc-python coc-explorer coc-git coc-json coc-tailwindcss coc-phpls coc-prettier coc-tsserver --global-style --ignore-scripts --no-bin-links --no-package-lock --only=prod
```

### For tmux

TPM is used to manage plugins. Install it with:

```bash
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

If you're using iTerm2 and want to be able to copy to the system clipboard from tmux's copy mode, then head to Preferences > General > Selection and make sure these are selected:

- [x] Copy to pasteboard on selection
- [x] Applications in terminal may access clipboard

Start a new tmux server and install the tpm plugins defined in `tmux.conf` with:

<kbd>Ctrl</kbd>+<kbd>b</kbd> <kbd>Shift</kbd>+<kbd>I</kbd>
